# Sintaxis dplyr para procesar encuestas en R

## Objetivo 

El objetivo de este repositorio es brindar una alternativa al procesamiento de encuestas con R, utilizando 
el paquete srvyr, una versión 'tidy' del paquete clásico survey.


## Dependencias

Vamos a usar los siguientes paquetes :

- **dplyr** (Para maniuplar los datos)

- **srvyr** (Para obtener resultados poblacionales.)

- **purrrr** (Para iterar y generar múltiples resultados, generar series, estimar dominios, etc.)

## Clonar este repositorio en forma local

Para clonar este repositorio, debes de abrir la terminal que utilices y escribir la siguiente línea de comandos:

``` bash

git clone <url>
cd <nombre-repositorio>
explorer . # (Si utilizas Windows)
```
Para luego, abrir el proyecto de RStudio (.Rproj)

## Datos

Utilizaremos los microdatos de la ECH (Encuesta Continua de Hogares) del Instituto Nacional de Estadística. Con esta encuesta podemos obtener indicadores sociodemográficos de interés general.

### Obtener datos

Todos los datos son descargados la página oficial del INE [https://www.ine.gub.uy/web/guest/encuesta-continua-de-hogares1](https://www.ine.gub.uy/web/guest/encuesta-continua-de-hogares1)

De todas maneras, se encuentran disponibles las encuestas ya descargadas en formato SAV (SPSS) comprimidas en un archivo zip, el mismo se encuentra en la raíz del repositorio.

Para trabajar de forma mas rápida, vamos a utilizar los datos en formato rds, para esto, debemos de correr el script config.R en la raíz del repositorio.

El mismo, va a descomprimir el archivo zip los guardara temporalmente en la carpeta data-raw para luego incluir los archivos rds en la carpeta data.