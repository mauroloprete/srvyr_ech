---
title : Jugando con srvyr y la ECH.
author : Ramón Alvarez-Vaz y Mauro Loprete
date : 04 de April
output :
    rmdformats::robobook:
        code_folding: show
        self_contained: true
        thumbnails: true
        lightbox: true
        gallery: true
        highlight: tango
---

```{r,echo = FALSE}
pacman::p_load(
    srvyr,
    purrr,
    stringr,
    haven,
    here,
    rlang
)
knitr::opts_chunk$set(warning = FALSE, message = FALSE) 
```




# Introducción

En base a lo que vimos en el curso, vamos a estimar tasas del mercado de trabajo, desagregando por sexo y clasificación por sector de actividad.

La idea es replicar las siguientes series historicas , a nivel de **total nacional** : 


-  [Tasa de Actividad Empleo y Desempleo por sexo (103)](https://www.ine.gub.uy/c/document_library/get_file?uuid=50ae926c-1ddc-4409-afc6-1fecf641e3d0&groupId=10181)
    Desde el año 2010 hasta el 2019

-  [Distribución de la población ocupada, por sector de actividad - CIIU4 (203)](https://www.ine.gub.uy/c/document_library/get_file?uuid=bd5c5c58-5e1b-4fc5-95b4-8529413b865e&groupId=10181)
    Desde el año 2010 hasta el 2019

-  Incidencia de la pobreza con la metodología 2006

# ¿Cómo lo hacemos?

Si bien, existen publicadas estas series podemos replicarlas con los microdatos publicados para terceros, si bien no podemos replicar 
los intervalos de confianza ya que no se tiene información de los estratos para calcular errores estandar.

## Paquetes que vamos a utilzar

```{r}

if(!require(pacman)) install.packages("pacman")
pacman::p_load(
    srvyr,
    purrr,
    stringr,
    haven,
    here,
    rlang,
    tibble
)

```

## Datos

Utilizaremos los microdatos de la ECH (Encuesta Continua de Hogares) del Instituto Nacional de Estadística. Con esta encuesta podemos obtener indicadores sociodemográficos de interés general.

### Obtener datos

Todos los datos son descargados la página oficial del INE [https://www.ine.gub.uy/web/guest/encuesta-continua-de-hogares1](https://www.ine.gub.uy/web/guest/encuesta-continua-de-hogares1)

De todas maneras, se encuentran disponibles las encuestas ya descargadas en formato SAV (SPSS) comprimidas en un archivo zip, el mismo se encuentra en la raíz del repositorio.

Para trabajar de forma mas rápida, vamos a utilizar los datos en formato rds, para esto, debemos de correr el script config.R en la raíz del repositorio.

El mismo, va a descomprimir el archivo zip los guardara temporalmente en la carpeta data-raw para luego incluir los archivos rds en la carpeta data.

Para esto, utilizamos el siguiente script : 

```{r,eval = FALSE}
source(
    here(
        "config.R"
    )
)
```

## Flujo de trabajo

A la hora de trabajar con encuestas, podemos dividir el proceso en los siguientes pasos : 

- Carga de la encuesta

```{r}

cargar_encuesta <- function(year) {
    readRDS(
        file = here(
            "data",
            paste0(
                "ech_",
                year,
                ".rds"
            )
        )
    )
}

```

Vamos a probarla para la edición 2019 : 
```{r}
cargar_encuesta(2019)
```


- Preprocesamiento de la encuesta

    Normalización de los metadatos, nombres de las variables, construcción de variables auxiliares.

    Si bien, esto es variable, para fines didacticos vamos a asumir que solo creamos variables auxiliares para realizar las estimaciones,
    que en este caso son variables indicadoras que si pertenecen al grupo de personas de la pea, pet ó desocupadas.

    Además, para evitar problemas en los nombres de las variables, vamos a modificar los nombres de las columnas transformando todo a minusculas.

```{r}

preprocesamiento <- function(svy,year) {
    svy  %>%
    set_names(
        tolower
    ) %>%
    mutate(
        pea = as.numeric(pobpcoac %in% 2:5),
        pet = as.numeric(pobpcoac != 1),
        po = as.numeric(pobpcoac == 2),
        pd = as.numeric(pobpcoac %in% 3:5),
        year = year,
        f72_2 = trunc(as.numeric(f72_2)/100),
        edad25 = as.numeric(e27 >= 25),
        sector_actividad = dplyr::case_when(
             f72_2 %in% 1:4 ~ "A",
             f72_2 %in% 5:9 ~ "B",
             f72_2 %in% 10:33 ~ "C",
             f72_2 == 35 ~ "D",
             f72_2 %in% 36:39 ~ "E",
             f72_2 %in% 41:43 ~ "F",
             f72_2 %in% 45:47 ~ "G",
             f72_2 %in% 49:53 ~ "H",
             f72_2 %in% 55:56 ~ "I",
             f72_2 %in% 58:63 ~ "J",
             f72_2 %in% 64:66 ~ "K",
             f72_2 == 68 ~ "L",
             f72_2 %in% 69:75 ~ "M",
             f72_2 %in% 77:82 ~ "N",
             f72_2 == 84 ~ "O",
             f72_2 == 85 ~ "P",
             f72_2 %in% 86:88 ~ "Q",
             f72_2 %in% 90:93 ~ "R",
             f72_2 %in% 94:96 ~ "S",
             f72_2 %in% 97:98 ~ "T",
             f72_2 == 99 ~ "U"
        )
    )
}


```



- Se establece el diseño 

    Para el cálculo de los errores estandar se necesita considerar el diseño.

```{r}

establecer_design <- function(svy) {
    svy  %>%
    as_survey_design(
        1,
        weight = pesoano,
        variables = everything()
    )
}
```


- Se realizan las estimaciones

    Dependiendo de la variable en la que estemos interesados, estimaremos totales, conteos, proporciones, medias, percentiles, etc.


```{r}

estimaciones_trabajo <- function(
    design,
    by
) {
    design %>%
    group_by(
        year,
        {{by}}
    ) %>%
    summarise(
        TA = survey_ratio(
            pea,
            pet,
            vartype = NULL
        ),
        TD = survey_ratio(
            pd,
            pea,
            vartype = NULL
        ),
        TE = survey_ratio(
            po,
            pet,
            vartype = NULL
        )
    )
}


estimaciones_ocupado <- function(
    design,
    by
) {
    design %>%
    group_by(
        year,
        {{by}}
    ) %>%
    summarise(
        ocupada = survey_total(
            po,
            vartype = NULL
        )
    ) %>%
    mutate(
        ocupada = ocupada/sum(ocupada)
    )
}

estimaciones_pobreza <- function(
    design,
    by
) {
    design %>%
    group_by(
        year,
        {{by}}
    ) %>%
    summarise(
        pobreza06 = survey_mean(
            pobre06,
            vartype = NULL
        )
    )
}

```


**La idea es :** 

Repetir este proceso para cada una de las ediciones de la encuesta, luego, añadir esta estimación en un 'vector' con las sucesivas estimaciones,
para esto utilizaremos las funciones map del paquete purrr.

Es decir, repetiremos esto : 

```{r}

cargar_encuesta(2019) %>%
preprocesamiento(2019) %>%
establecer_design() %>%
estimaciones_trabajo(
    by = e26
)

```

## Funciones map

Las funciones map son útiles cuando deseamos aplicar una función a un vector, matrix (data.frame) de datos, es decir, es como si aplicaramos una función apply 
o una función vectorizada de R.

Si bien estas opciones se encuentran disponibles en R base (las familia de funciones apply), purrrr es un paquete que nos permite aplicarlos de forma
comoda y siguiendo la filosofía del **tidyverse**. 

Existen differentes funciones map, su variabilidad depende de la cantidad de argumentos que tiene la función que deseamos aplicar,
en el caso de tener uno, utilizamos la familia map_*, para aplicaciones de dos argumentos utilizaremos la familia map_2* y para tres o más argumentos
utilizaremos aquellas funciones del estilo pmap_*. 

Además de tener en cuenta la cantidad de argumentos, debemos de tener en cuenta la salida que deseamos obtener, es decir la salida, ¿será un data.frame?, ¿un vector númerico entero?,
¿un string?, etc.

En nuestro caso utilizaremos la opción de que la salida sea un data.frame, mas precisamente un tibble. Esto es ya que a cada archivo de la encuesta le aplicaremos 
una primera parte de procesamiento, luego haremos una estimación y la almacenaremos en la tabla que iremos construyendo de forma iterativa.


## Validación por nombres : 

```{r}
validar_serie <- function(
    .svy,
    .vars
) {
    map_dfr(
        .svy,
        .f = function(svy) {
            cargar_encuesta(.svy) %>%
            set_names(
                tolower
            ) %>%
            names() %>%
            assign(
                "svy_name",
                .,
                envir = parent.env(
                    environment()
                )
            )

            tibble(
                Variable = .vars,
                Validacion = .vars %in% svy_name,
                year = rep(
                    svy,
                    length(
                        .vars
                    )
                )
            )
        }
    )
}

map_dfr(
    2010:2019,
    .f = ~ validar_serie(
        .x,
        .vars = c(
            "pobpcoac",
            "e26", 
            "e27", 
            "pesoano",
            "f72_2",
            "pobre06"
        )
    )
) %>%
gt::gt()

```

Haciendo una primera validación, vemos que la variable e26 no se encuentra en la edición 2007 y 2008, que si bien hacemos una inspección del diccionario y del 
recorrido de la variable, podemos ver que estan corridas un número, es decir la variable sexo es e27 y e28 es la edad.

Este tipo de problemas ocurre al estar trabajando con encuestas de varios años, si bien esto es solucionable para no perder tiempo, estimaremos la serie a partir de 
2009.


# Tasa de Empleo y desempleo por sexo (103)

En nuestro caso, al que nuestra salida en cada encuesta es un data.frame y para una mayor comodidad, queremos tener los datos guardados en un tibble, por lo tanto
usaremos el sufijo _dfr., estimaremos las tasa del mercado de trabajo para hombres y para mujeres, por lo tanto usaremos la variable e26 para agrupar.

```{r}

2010:2019 %>%
map_dfr(
    .f = function(x) {
        cargar_encuesta(x) %>%
        preprocesamiento(x) %>%
        establecer_design() %>%
        estimaciones_trabajo(
            by = e26
        )
    }
) %>%
mutate(
    e26 = ifelse(
        e26 == 1,
        "Hombres",
        "Mujeres"
    )
) %>%
gt::gt(
    groupname_col = NULL
) %>%
gt::fmt_percent(
    columns = 3:5,
    decimals = 1
)

```


# Tasa de actividad por sector de actividad (203)


```{r}


2010:2019 %>%
map_dfr(
    .f = function(x) {
        cargar_encuesta(x) %>%
        preprocesamiento(x) %>%
        establecer_design() %>%
        estimaciones_ocupado(
            by = sector_actividad
        )
    }
) %>%
gt::gt(
    groupname_col = NULL
) %>%
gt::fmt_percent(
    columns = 3,
    decimals = 1
)
```


# Incidencia de la pobreza con la metodología 2006

```{r}
2010:2019 %>%
map_dfr(
    .f = function(x) {
        cargar_encuesta(x) %>%
        preprocesamiento(x) %>%
        establecer_design() %>%
        estimaciones_pobreza()
    }
) %>%
gt::gt(
    groupname_col = NULL
) %>%
gt::fmt_percent(
    columns = 2,
    decimals = 1
)
```





